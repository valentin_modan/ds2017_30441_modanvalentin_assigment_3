package consumer;


import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import com.rabbitmq.client.*;

import dvd.Dvd;





public class ConsumerClass {

	public static int FILE_NUMBER = 1;
	
	private static final String EXCHANGE_NAME = "logs";
    public static void writeToFile(String filename,String message){
        PrintWriter writer;
   
            try {
				writer = new PrintWriter(filename+".txt", "UTF-8");
				writer.println(message);
		        writer.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
          
       
    }
    
    public static String openFileToString(byte[] _bytes)
    {
        String file_string = "";

        for(int i = 0; i < _bytes.length; i++)
        {
            file_string += (char)_bytes[i];
        }

        return file_string;    
    }
    
    public static void writeToFile(String message) {
        BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter("file_"+FILE_NUMBER++ +".txt"));
	        writer.write(message);
	        writer.close();
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        
        
    }
    
    public static void sendMailToUser(String message) {
    	
    	
    	MailService mailService = new MailService(ClientData.username, ClientData.password);
		mailService.sendMail(ClientData.username,"Dummy Mail Title",message);
    }
    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

       Consumer consumer = new DefaultConsumer(channel) {
    	   @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
    		  
    		   	
    		   String message = new String(body, "UTF-8");
    		 //  Dvd message =   (Dvd) SerializationUtils.deserialize(body); 
    		   //String message = openFileToString(body);
                System.out.println(" [x] Received in consumer with files'" + message.toString() + "'");
                
                writeToFile(message);
                
                sendMailToUser(message);
              
            }
        };
        
        
        
        channel.basicConsume(queueName, true, consumer);
    }
	
}
