package dvd;

import java.io.Serializable;

public class Dvd implements Serializable{

	private String Title;
	private int Year;
	private double Price;
	

	public Dvd(String title, int year, double price) {
		super();
		Title = title;
		Year = year;
		Price = price;
	}
	
	public Dvd()
	{
		Title="empty";
		Year = 0;
		Price = 0;
	}


	public String getTitle() {
		return Title;
	}


	public void setTitle(String title) {
		Title = title;
	}

	public int getYear() {
		return Year;
	}

	public void setYear(int year) {
		Year = year;
	}


	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}



	@Override
	public String toString() {
		return "DVD [Title=" + Title + ", Year=" + Year + ", Price=" + Price + "]";
	}
	
	
}
