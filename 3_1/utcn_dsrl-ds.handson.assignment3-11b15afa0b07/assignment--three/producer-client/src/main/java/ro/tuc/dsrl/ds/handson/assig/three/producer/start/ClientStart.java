package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import DVD.DVD;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
		
			List<DVD> dvdList = new ArrayList<DVD>();
			dvdList.add(new DVD("Starwars",2017,15.5));
			dvdList.add(new DVD("Breaking Bad",2014,10.5));
			dvdList.add(new DVD("Star Trek",2012,11.5));
			dvdList.add(new DVD("Game of Thrones",2010,14.5));
			
		
			for (int i=0;i<4;i++) {
				queue.writeMessage(dvdList.get(i).toString()+" "+i);
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
