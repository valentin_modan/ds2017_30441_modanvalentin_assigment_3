package producer;

import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import dvd.Dvd;

public class Producer {

	
	
	private static final String EXCHANGE_NAME = "logs";

    public void sendMessage(Dvd dvd)
            throws java.io.IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

       
      //  byte[] data = SerializationUtils.serialize(dvd);
        //data
        String message = dvd.toString();
        byte[] data = message.getBytes();
        channel.basicPublish(EXCHANGE_NAME, "", null, data);
        System.out.println(" [x] Sent '" + message + "'");

        channel.close();
        connection.close();
    }
    
}
