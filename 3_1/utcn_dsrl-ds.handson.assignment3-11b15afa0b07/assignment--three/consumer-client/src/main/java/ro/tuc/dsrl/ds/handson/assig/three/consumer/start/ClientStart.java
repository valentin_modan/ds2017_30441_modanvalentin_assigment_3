package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {
	
	static int i=1;

	private ClientStart() {
	}
	

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost",8888);

		MailService mailService = new MailService(ClientData.username, ClientData.password);
		String message;

		while(true) {
			try {
				message = queue.readMessage();
				System.out.println("Sendring mail "+message);
				
				
				BufferedWriter writer = new BufferedWriter(new FileWriter("File" + i++ +".txt"));
				writer.write(message);
				writer.close();
				
				
				System.out.println("Done");
				//	MailService mailService = new MailService(ClientData.username, ClientData.password);
				mailService.sendMail(ClientData.username,"Dummy Mail Title",message);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("[ERROR]Something went wrong");
			}
		}
	}
}
